# :space_invader: Pencil

React native application for playing pencil.space

## :computer: Contributing
- Get the latest version of react-native installed on your machine. Follow steps at: [reactnative.dev](https://reactnative.dev)
- Once the setup is done you can launch the simulator by
```
yarn ios # or yarn android
```

## :memo: Misc
- Make sure you run `pod install` in the ios folder. The svg dependency requires it.

## :bookmark: License

This project is [MIT](LICENSE) licensed.

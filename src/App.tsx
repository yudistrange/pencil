/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {Component} from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import {Tarp} from './components/Tarp';
import {connect, createChannel, joinRoom} from './net/Socket';
import {Socket, Channel} from 'phoenix';
import {ChatBox} from './components/ChatBox';

interface IProps {}
interface IState {
  socket: Socket;
  channel: Channel;
}

class App extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    const url = 'http://localhost:4000/socket';
    const socket = connect(url, {});
    const channel = createChannel(socket, 'room:lobby');
    joinRoom(channel);
    this.state = {socket: socket, channel: channel};
  }

  render() {
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView style={appStyle}>
          <Tarp channel={this.state.channel} />
          <ChatBox channel={this.state.channel} />
        </SafeAreaView>
      </>
    );
  }
}

const appStyle = {
  flex: 1,
  backgroundColor: 'lightblue',
};

export default App;

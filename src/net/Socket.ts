import {Socket, Channel} from 'phoenix';

function connect(connectionUrl: string, token: {}): Socket {
  const socket = new Socket(connectionUrl, token);
  socket.onOpen(() => console.log('Socket Connected..'));
  socket.onError((event) => console.error('Socket failed with error: ', event));
  socket.onClose(() => console.log('Socket closing. Goodbye!'));
  socket.connect();
  return socket;
}

function createChannel(socket: Socket, roomName: string): Channel {
  return socket.channel(roomName, {});
}

function joinRoom(channel: Channel) {
  channel.join();
  channel.onError((event) => console.error('Channel error: ', event));
  channel.onClose((event) => console.log('Channel close event: ', event));
}

export {connect, createChannel, joinRoom};

import React, {Component} from 'react';
import {View, Dimensions} from 'react-native';
import {Svg, Path} from 'react-native-svg';
import {Channel} from 'phoenix';

interface ITarpState {
  isNewTouch: boolean;
  touches: string;
}

interface ITarpProps {
  channel: Channel;
}

interface ITouch {
  locationY: Number;
  locationX: Number;
}

export class Tarp extends Component<ITarpProps, ITarpState> {
  constructor(props: ITarpProps) {
    super(props);
    this.state = {
      isNewTouch: false,
      touches: '',
    };
  }

  componentDidMount() {
    this.props.channel.on('[DRAW]', this.onDrawReceive.bind(this));
  }

  onDrawReceive(payload: any) {
    console.log(payload);
    if (payload !== undefined) {
      this.setState((prevState, _props) => {
        return {touches: prevState.touches + payload.message};
      });
    }
    return payload;
  }

  onStartShouldSetResponder = (_evt: Event) => {
    return true;
  };

  onMoveShouldSetResponder = (_evt: Event) => {
    console.log('I am not a responder yet');
    return true;
  };

  onResponderMove = (evt: Event) => {
    const touch: ITouch = evt.nativeEvent.touches[0];
    const newTouches = positionToSvg(
      touch.locationX,
      touch.locationY,
      this.state.isNewTouch,
    );

    this.setState((_prevState) => {
      return {
        isNewTouch: false,
        touches: this.state.touches + newTouches,
      };
    });
    this.props.channel.push('[DRAW]', {message: newTouches});
  };

  onResponderGrant = (_evt: Event) => {
    console.log('Responder status granted');
    this.setState({isNewTouch: true});
  };

  onResponderReject = (_evt: Event) => {
    console.error('Responder status denied');
  };

  render() {
    const path = this.state.touches;
    console.log('path:', path);
    console.log(
      'Dimension:',
      Dimensions.get('window').width,
      Dimensions.get('window').height,
    );

    return (
      <View
        style={tarpStyle}
        onStartShouldSetResponder={this.onStartShouldSetResponder}
        onMoveShouldSetResponder={this.onMoveShouldSetResponder}
        onResponderMove={this.onResponderMove}
        onResponderGrant={this.onResponderGrant}
        onResponderReject={this.onResponderReject}>
        {/* <Text>{path}</Text> */}
        <Svg height="100%" width="100%" viewBox={tarpDimensions()}>
          <Path d={path} fill="none" stroke="red" />
        </Svg>
      </View>
    );
  }
}

const margin = 15;

const tarpStyle = {
  backgroundColor: 'white',
  borderColor: 'black',
  borderRadius: 5,
  borderWidth: 2,
  margin: margin,
  width: Dimensions.get('window').width - 2 * margin,
  height: Dimensions.get('window').height * 0.6 - margin,
};

const positionToSvg = (
  posX: Number,
  posY: Number,
  isNewTouch: Boolean,
): string => {
  if (isNewTouch) {
    return 'M ' + posX + ' ' + posY + ' ';
  } else {
    return 'L ' + posX + ' ' + posY + ' ';
  }
};

const tarpDimensions = () => {
  const width = Dimensions.get('window').width - 2 * margin;
  const height = Dimensions.get('window').height / 2 - 15;

  console.log('height: ' + height + ' width: ' + width);

  return '15 15 ' + width + ' ' + height;
};

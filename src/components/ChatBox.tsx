import React, {Component} from 'react';
import {View, Text, Dimensions} from 'react-native';
import {Channel} from 'phoenix';
import ChatDisplay from './ChatDisplay';
import ChatInput from './ChatInput';

interface IChatBoxProps {
  channel: Channel;
}
interface IChatBoxState {
  chats: String[];
}

export class ChatBox extends Component<IChatBoxProps, IChatBoxState> {
  constructor(props: IChatBoxProps) {
    super(props);
    this.state = {
      chats: [],
    };
  }

  componentDidMount() {
    this.props.channel.on('[CHAT]', this.onChatReceive.bind(this));
  }

  onChatReceive(payload: any) {
    this.setState((prevState, _props) => {
      return {chats: prevState.chats.concat(payload.message)};
    });
  }

  render() {
    return (
      <View style={chatBoxStyle}>
        <Text style={chatHeaaderStyle}>Chats:</Text>
        <ChatDisplay chats={this.state.chats} />
        <ChatInput channel={this.props.channel} />
      </View>
    );
  }
}

const heightMargin = Math.ceil(Dimensions.get('window').height * 0.03);
const widthMargin = Math.ceil(Dimensions.get('window').width * 0.03);

const chatBoxStyle = {
  backgroundColor: 'white',
  marginLeft: widthMargin,
  marginRight: widthMargin,
  marginBottom: heightMargin,
  width: Dimensions.get('window').width - 2 * widthMargin,
  height: Dimensions.get('window').height * 0.3 - heightMargin,
};

const chatHeaaderStyle = {
  borderWidth: 2,
  padding: 5,
  borderRadius: 5,
  fontWeight: 'bold',
  fontSize: 15,
};

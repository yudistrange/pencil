import React, {Component} from 'react';
import {Channel} from 'phoenix';
import {TextInput} from 'react-native';

interface IChatInputProps {
  channel: Channel;
}
interface IChatInputState {
  value: string;
}
export default class ChatInput extends Component<
  IChatInputProps,
  IChatInputState
> {
  constructor(props: IChatInputProps) {
    super(props);
    this.state = {
      value: '',
    };
  }

  onChangeHandler(text: string) {
    console.log('text: ', text);
    console.log('value: ', this.state.value);
    this.setState((_state, _props) => {
      return {value: text};
    });
    this.props.channel.push('[CHAT]', {message: text});
  }

  render() {
    return (
      <TextInput
        style={chatInputStyle}
        value={this.state.value}
        onChangeText={(text) => this.onChangeHandler(text)}
      />
    );
  }
}

const chatInputStyle = {
  padding: 10,
  borderColor: 'black',
  borderWidth: 2,
  borderRadius: 5,
};

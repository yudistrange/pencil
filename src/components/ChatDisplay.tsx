import React from 'react';
import {Dimensions, Text, ScrollView} from 'react-native';

interface IChatProps {
  chats: String[];
}
const ChatDisplay = (props: IChatProps) => {
  return (
    <ScrollView style={chatDisplayComponentStyle}>
      {props.chats.map((chat, i) => (
        <Text key={i} style={chatDisplayStyle}>
          {chat}
        </Text>
      ))}
    </ScrollView>
  );
};

const chatDisplayStyle = {marginLeft: 4, fontSize: 15};

const height = Dimensions.get('window').height * 0.1;
const chatDisplayComponentStyle = {
  backgroundColor: 'white',
  height: height,
  borderColor: 'black',
  borderWidth: 2,
  borderRadius: 5,
};

export default ChatDisplay;
